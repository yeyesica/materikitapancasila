/* eslint-disable global-require */
const images = {
  logo: require('../../assets/images/logo.png'),
  logo2: require('../../assets/images/logo2.png'),
  setting: require('../../assets/images/setting.png'),
  background: require('../../assets/images/background.png'),
  backgroundv2: require('../../assets/images/backgroundv2.png'),
  bab1: require('../../assets/images/buttonbab1.png'),
  bab2: require('../../assets/images/buttonbab2.png'),
  bab3: require('../../assets/images/buttonbab3.png'),
  bab4: require('../../assets/images/buttonbab4.png'),
  bab5: require('../../assets/images/buttonbab5.png'),
  bab6: require('../../assets/images/buttonbab6.png'),
  buttonKuis: require('../../assets/images/btnKuis.png'),
  buttonMateri: require('../../assets/images/btnMateri.png'),
  buttonVideo: require('../../assets/images/btnVideo.png'),
  buttonArtikel: require('../../assets/images/btnArtikel.png'),
  buttonMasuk: require('../../assets/images/btnMasuk.png'),
  buttonDaftar: require('../../assets/images/btnDaftar.png'),
  buttonLatSoal: require('../../assets/images/btnLatihanSoal.png'),
  buttonUjian: require('../../assets/images/btnUjian.png'),
  buttonBack: require('../../assets/images/ButtonBack.png'),
  garuda: require('../../assets/images/garuda.jpg')
  // back: require('../../assets/images/back.png'),
  // checkBoxOn: require('../../assets/images/checkbox_on.png'),
  // checkBoxOff: require('../../assets/images/checkbox_off.png'),
  // close: require('../../assets/images/close.png'),
  // dropdown: require('../../assets/images/ic_dropdown.png'),
  // faceSad: require('../../assets/images/face_sad.svg'),
  // login: require('../../assets/images/login.png'),
  // modalCamera: require('../../assets/images/modal_camera.png'),
  // modalGallery: require('../../assets/images/modal_gallery.png'),
  // noConnection: require('../../assets/images/no_connection.png'),
  // onBoarding: [
  //   require('../../assets/images/onBoarding_0.png'),
  //   require('../../assets/images/onBoarding_1.jpg'),
  //   require('../../assets/images/onBoarding_2.png')
  // ],
  // passOff: require('../../assets/images/padlock_off.png'),
  // passOn: require('../../assets/images/padlock_on.png'),
  // search: require('../../assets/images/ic_search.png'),
  // userOff: require('../../assets/images/user_off.png'),
  // userOn: require('../../assets/images/user_on.png')
};

export default images;
