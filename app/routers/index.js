import { SwitchNavigator } from 'react-navigation';
import { SplashScreenStack, LoginStack, SignUpStack, HomePageStack, KuisStack, ListArtikelStack, VideoStack, MateriStack, Bab1Stack, Bab2Stack, Bab3Stack, Bab4Stack, Bab5Stack, Bab6Stack } from './stackNavigator';
import { Drawer } from './drawerNavigator';

export default SwitchNavigator(
  {
    SplashScreen: SplashScreenStack,
    Login: LoginStack,
    SignUp: SignUpStack,
    HomePage: HomePageStack,
    ListArtikel: ListArtikelStack,
    Kuis: KuisStack,
    Video: VideoStack,
    Materi: MateriStack,
    Bab1: Bab1Stack,
    Bab2: Bab2Stack,
    Bab3: Bab3Stack,
    Bab4: Bab4Stack,
    Bab5: Bab5Stack,
    Bab6: Bab6Stack,
    App: Drawer
  },
  {
    initialRouteName: 'Bab3'
  }
);
