import { StyleSheet } from 'react-native';
import { COLOR_WHITE, COLOR_BLACK } from '../../styles';
import { scale } from '../../utils/scaling';
import METRICS from '../../constants/metrics';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR_WHITE
  },
  logoContainer: { 
    width: scale(150), 
    height: scale(150) 
  },
  logo: { 
    width: '100%', 
    height: '100%', 
    marginBottom: METRICS.baseMargin
  }
});

export default styles;
