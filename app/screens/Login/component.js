/* eslint-disable no-shadow */
/* eslint-disable no-undef */
/* eslint-disable no-console */
/* eslint-disable no-alert */
import React from 'react';
import { Text, View, Image, ToastAndroid, TextInput, TouchableOpacity, ImageBackground, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import MainScreen from '../../components/layouts/MainScreen';
import styles from './styles';
import axios from 'axios';
import I18n from '../../i18n';
import IMAGES from '../../configs/images';
import Button from '../../components/elements/Button';
import { ENDPOINT } from '../../configs';

export default class Component extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      checked: false,
      showPass: true,
      email: '',
      password: '',
      disableButton: true,
      isHidden: true,
      isLoading: false
    };
  }
  // _handleLogin = async () => {
  //   this.props.navigation.navigate('Menu');
  // };
  _handleRegister = async () => {
    this.props.navigation.navigate('SignUp');
  };
  // _handleLogin = async () => {
  //   const { email, password } = this.state;
  //   const params = {
  //     email,
  //     password
  //   };
  //   await ENDPOINT.login(params);
  //   this.props.navigation.navigate('HomePage');
  // };

  _handleLogin = async () => {
    const { email, password } = this.state;
    const params = { email, password };
    try {
      this.setState({ isLoading: true });
      const result = await ENDPOINT.login(params);
      if (result.code === 200) {
        await storage.set(STORAGE_KEY.TOKEN_LOGIN, result.data);
        this.props.navigation.navigate('HomePage');
      } else {
        ToastAndroid.show('Failed to login', ToastAndroid.SHORT);
      }
    } catch (error) {
      this.props.navigation.navigate('HomePage');
    } finally {
      this.setState({ isLoading: false });
    }
  };

  _handleUsername = async text => {
    await this.setState({ email: text });
    this.checkField();
  };
  _handlePassword = async text => {
    await this.setState({ password: text });
    this.checkField();
  };
  checkField() {
    const { email, password } = this.state;
    if (email === '' || password === '') {
      this.setState({ disableButton: true });
    } else {
      this.setState({ disableButton: false });
    }
  }

  render() {
    const { email, password } = this.state;
    return (
      <ImageBackground source={IMAGES.background} style={{width: '100%', height: '100%'}}>
        <StatusBar hidden />
        <MainScreen style={styles.mainContainer}>
            <View style={styles.logoContainer}>
              <Image source={IMAGES.logo} resizeMode="contain" style={styles.logo} />
            </View>
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderWidth: 2, borderRadius: 5, backgroundColor: '#FBFCFE', margin: 15, padding: 10}}
              placeholder="Email"
              editable
              isRequired={true}
              value={email}
              onChangeText={this._handleUsername}
            />
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderRadius: 5, borderWidth: 2, backgroundColor: '#FBFCFE', marginBottom: 15, padding: 10}}
              placeholder="Kata sandi"
              editable
              isRequired={true}
              secureTextEntry={true}
              value={password}
              onChangeText={this._handlePassword}
            />
            <TouchableOpacity onPress={this._handleLogin}>
              <Image source={IMAGES.buttonMasuk} style={styles.btnMasuk} resizeMode="contain" />
            </TouchableOpacity>
            <Text style={{marginBottom: 30}}>Belum punya akun?<Text style={{fontWeight: 'bold', color: '#3ABFDC'}} onPress={this._handleRegister}> Daftar sekarang</Text></Text>    
        </MainScreen>
      </ImageBackground>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
