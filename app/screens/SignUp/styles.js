import { StyleSheet } from 'react-native';
import { COLOR_WHITE, COLOR_BLACK, COLOR_BASE_PRIMARY_MAIN } from '../../styles';
import { scale } from '../../utils/scaling';
import METRICS from '../../constants/metrics';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginTop: 60,
    alignItems: 'center'
  },
  logoContainer: { width: scale(540), height: scale(90) },
  logo: { flex: 1, width: undefined, height: undefined, marginBottom: 35 },
  appTitle: { fontSize: 18, color: COLOR_BLACK, fontWeight: 'bold', margin: 10 },
  account: {
    alignSelf: 'center',
    marginBottom: METRICS.doubleSection
  },
  margin: { marginBottom: METRICS.baseMargin },
  outlined: { backgroundColor: COLOR_WHITE, borderColor: COLOR_BASE_PRIMARY_MAIN, borderWidth: 2 },
  outlinedText: { color: COLOR_BASE_PRIMARY_MAIN },
  btnMasuk: {
    width: 500, 
    height: 48,
    marginTop: 20,
    marginBottom: 10
  }
});

export default styles;
