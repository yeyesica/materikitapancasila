/* eslint-disable no-shadow */
/* eslint-disable no-undef */
/* eslint-disable no-console */
/* eslint-disable no-alert */
import React from 'react';
import { Text, View, Image, ToastAndroid, TextInput, TouchableOpacity, ImageBackground, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import MainScreen from '../../components/layouts/MainScreen';
import styles from './styles';
import axios from 'axios';
import I18n from '../../i18n';
import IMAGES from '../../configs/images';
import Button from '../../components/elements/Button';
import { ENDPOINT } from '../../configs';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.login = this.login.bind(this);
  }
  login = async () => {
    const {email, password} = this.state;
    const payload = {
      email: email,
      password: password,
    };
    axios //ada put, post, delete, get
      //post butuh 2 parameter, url + data(payload)
      .post('http://ec2-3-81-168-96.compute-1.amazonaws.com/api/register', payload)
      .then(async value => {
        await AsyncStorage.setItem('access_token', value.data.access_token);
        ToastAndroid.show('Success', ToastAndroid.SHORT);
        this.props.navigation.navigate('HomePage');
      })
      .catch(err => {
        ToastAndroid.show('Error', ToastAndroid.SHORT);
        console.log(err);
      });
  };

  signIn = () => {
    this.props.navigation.navigate('Login');
  };

  tekan = () => {
    this.props.navigation.navigate('HomePage');
  };

  render() {
    const { email, password } = this.state;
    return (
      <ImageBackground source={IMAGES.background} style={{width: '100%', height: '100%'}}>
        <StatusBar hidden />
        <MainScreen style={styles.mainContainer}>
            <View style={styles.logoContainer}>
              <Image source={IMAGES.logo2} resizeMode="contain" style={styles.logo} />
            </View>
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderWidth: 2, borderRadius: 5, backgroundColor: '#FBFCFE', padding: 10}}
              placeholder="Nama Lengkap"
              editable
            />
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderWidth: 2, borderRadius: 5, backgroundColor: '#FBFCFE', margin: 15, padding: 10}}
              placeholder="Email"
              editable
              value={email}
              onChangeText={value => this.setState({email: value})}
            />
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderRadius: 5, borderWidth: 2, backgroundColor: '#FBFCFE', marginBottom: 15, padding: 10}}
              placeholder="Kata sandi"
              editable
              secureTextEntry={true}
              value={password}
              onChangeText={value => this.setState({password: value})}
            />
            <TextInput
              underlineColorAndroid="transparent"
              style={{width: '80%', height: 50, borderColor: '#CCCCCC', borderRadius: 5, borderWidth: 2, backgroundColor: '#FBFCFE', padding: 10}}
              placeholder="Konfirmasi kata sandi"
              editable
              secureTextEntry={true}
              value={password}
              onChangeText={value => this.setState({password: value})}
            />
            <TouchableOpacity onPress={this.tekan}>
              <Image source={IMAGES.buttonDaftar} style={styles.btnMasuk} resizeMode="contain" />
            </TouchableOpacity>
            <Text style={{marginBottom: 30}}>Sudah punya akun?<Text style={{fontWeight: 'bold', color: '#3ABFDC'}} onPress={this.signIn}> Masuk</Text></Text>    
        </MainScreen>
      </ImageBackground>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
