import React from 'react';
import { View, Image, Text, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import IMAGES from '../../configs/images';
import ButtonKuis from '../../components/elements/ButtonKuis';
import ButtonMateri from '../../components/elements/ButtonMateri';

export default class Component extends React.Component {
  onPressSetting = () => {
    this.props.navigation.navigate('Setting');
  };
  onPressKuis = () => {
    this.props.navigation.navigate('Kuis');
  };

  onPressMateri = () => {
    this.props.navigation.navigate('Materi');
  };

  render() {
    return (
      <ImageBackground source={IMAGES.background} style={{width: '100%', height: '100%'}}>
      <StatusBar hidden />
        <View style={{ flex: 1 }}>
          <StatusBar hidden />
          <TouchableOpacity style={{ padding: 20, alignItems: 'flex-end' }} onPress={this.onPressSetting}>
            <Image source={IMAGES.setting} resizeMode="contain" style={styles.setting} />
          </TouchableOpacity>
          
          <View style={{ paddingTop: 40, alignItems: 'center' }}>
            <Image source={IMAGES.logo} resizeMode="contain" style={styles.logo} />
            <TouchableOpacity onPress={this.onPressKuis}>
              <Image source={IMAGES.buttonKuis} style={styles.btnKuis} resizeMode="contain" />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onPressMateri}>
              <Image source={IMAGES.buttonMateri} style={styles.btnMateri} resizeMode="contain" />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
