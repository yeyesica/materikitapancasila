import React from 'react';
import { View, Image, Text, StatusBar, TouchableOpacity, ImageBackground } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import styles from './styles';
import PropTypes from 'prop-types';
import IMAGES from '../../configs/images';
import ButtonKuis from '../../components/elements/ButtonKuis';
import ButtonMateri from '../../components/elements/ButtonMateri';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      showAlertLatSoal: false,
      showAlertUjian: false };
  };
 
  showAlertLatSoal = () => {
    this.setState({
      showAlertLatSoal: true
    });
  };

  showAlertUjian = () => {
    this.setState({
      showAlertUjian: true
    });
  };
 
  hideAlertLatSoal = () => {
    this.setState({
      showAlertLatSoal: false
    });
  };

  hideAlertUjian = () => {
    this.setState({
      showAlertUjian: false
    });
  };

  onBack = () => {
    this.props.navigation.navigate('HomePage');
  };
  onLatSoal = () => {
    this.props.navigation.navigate('LatihanSoal');
  };
  onUjian = () => {
    this.props.navigation.navigate('Ujian');
  };

  render() {
    const {showAlertLatSoal, showAlertUjian} = this.state;
    return (
      <ImageBackground source={IMAGES.background} style={{width: '100%', height: '100%'}}>
      <StatusBar hidden />
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{ width: '20%'}}>
          <TouchableOpacity style={{ padding: 15, alignItems: 'flex-start' }} onPress={this.onBack}>
            <Image source={IMAGES.buttonBack} resizeMode="contain" style={styles.buttonBack}/>
          </TouchableOpacity>
        </View>
        <View style={{ width: '70%', marginTop: 20, marginLeft: 85}}>
          <Text style={{fontWeight: 'bold', fontSize: 25, color: '#000'}}>KUIS</Text>
        </View>
      </View>

      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 15}}>
        <View style={{ width: '50%'}}>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this.showAlertLatSoal(); }}>
            <Image source={IMAGES.buttonLatSoal} style={{width: 156, height: 164, margin: 15}} resizeMode="contain" />
          </TouchableOpacity>
        </View>
        <View style={{ width: '50%'}}>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => { this.showAlertUjian(); }}>
            <Image source={IMAGES.buttonUjian} style={{width: 156, height: 164, margin: 15}} resizeMode="contain" />
          </TouchableOpacity>
        </View>
      </View>

      <AwesomeAlert
          show={showAlertLatSoal}
          showProgress={false}
          title="Latihan Soal"
          titleStyle="bold"
          message="Jumlah Soal: 15, Waktu: - "
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Mulai"
          confirmButtonColor="#47E185"
          onCancelPressed={() => {
            this.hideAlertLatSoal();
          }}
          onConfirmPressed={() => {
            this.hideAlertLatSoal();
          }}
        />

        <AwesomeAlert
          show={showAlertUjian}
          showProgress={false}
          title="Ujian"
          message="Jumlah Soal: 10, Waktu: 2.5 menit"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Mulai"
          confirmButtonColor="#47E185"
          onCancelPressed={() => {
            this.hideAlertUjian();
          }}
          onConfirmPressed={() => {
            this.hideAlertUjian();
          }}
        />
      </ImageBackground>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
