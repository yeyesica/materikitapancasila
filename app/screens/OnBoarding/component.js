import React from 'react';
import { View, Image, Text, StatusBar } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import IMAGES from '../../configs/images';
import ButtonKuis from '../../components/elements/ButtonKuis';
import ButtonMateri from '../../components/elements/ButtonMateri';

export default class Component extends React.Component {
  _onPress = () => {
    this.props.navigation.navigate('Home');
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden />
        <View style={{ padding: 15, alignItems: 'flex-end' }}>
          <Image source={IMAGES.setting} resizeMode="contain" style={styles.setting} />
        </View>
        
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={IMAGES.logo} resizeMode="contain" style={styles.logo} />
          <ButtonKuis type="raised-ripple" title="Mulai Kuis" onPress={this._onPress}/>
          <ButtonMateri type="raised-ripple" title="Materi" onPress={this._onPress}/>
        </View>
      </View>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};
