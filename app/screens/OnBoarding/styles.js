import { StyleSheet } from 'react-native';
import { COLOR_BASE_PRIMARY_MAIN, COLOR_BLACK } from '../../styles';
import { scale } from '../../utils/scaling';
import METRICS from '../../constants/metrics';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  text: {
    fontSize: 20
  },
  logo: { 
    width: scale(150), 
    height: scale(150),
    marginBottom: 50
  },
  setting: {
    justifyContent: 'flex-end',
    width: scale(40),
    height: scale(40)
  },
  appTitle: { 
    fontSize: 17, 
    color: COLOR_BLACK, 
    fontWeight: 'bold'
  }
});

export default styles;
