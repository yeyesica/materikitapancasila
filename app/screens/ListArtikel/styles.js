import { StyleSheet } from 'react-native';
import { scale } from '../../utils/scaling';
import { COLOR_BLACK } from '../../styles';
import METRICS from '../../constants/metrics';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 20
  },
  buttonBack: {
    justifyContent: 'flex-end',
    width: scale(40),
    height: scale(40)
  },
  title: {
    marginTop: 20,
    marginLeft: 40,
    fontSize: 25,
    fontWeight: 'bold',
    color: COLOR_BLACK
  },
  btnBab: {
    width: scale(850),
    height: scale(80),
    margin: 10
  }
});

export default styles;
