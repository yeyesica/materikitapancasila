import React from "react";
import Svg, { Rect, Path } from "react-native-svg";

const SvgButtonKuis= props => (
	<Svg width={280} height={50} viewBox="0 0 280 50" fill="none" {...props}>
	  <Rect width={280} height={50} rx={6} fill="#E07C43" />
	  <Path d="M0 6a6 6 0 016-6h266v38a4 4 0 01-4 4H0V6z" fill="#FA9054" />
	</Svg>
);

export default SvgButtonKuis;
